package com.service;

import static org.junit.Assert.assertEquals;

import java.nio.file.Files;
import java.nio.file.Paths;

import javax.json.JsonObject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.Utils;

/**
 * These tests make sure that the service layer methods are working according to
 * spec.
 *
 * @author afshad
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc()
@DirtiesContext
public class ServiceTest {

	@Autowired
	private PriceService service;

	@Test
	public void testGetClosePriceSuccess() throws Exception {
		final String tickersymbol = "aapl";
		final String startDate = "2017-01-02";
		final String endDate = "2017-01-05";
		final String expected = "{\"Prices\":[{\"Ticker\":\"AAPL\",\"DateClose\":"
				+ "[[\"2017-01-03\",\"116.15\"],[\"2017-01-04\",\"116.02\"],[\"2017-01-05\",\"116.61\"]]}]}";

		JsonObject closePrice = service.getClosePrice(tickersymbol, startDate, endDate);
		String actual = Utils.jsonObjectToString(closePrice);
		assertEquals(expected, actual);
	}

	@Test
	public void testGetClosePriceNotFound() throws Exception {
		final String tickersymbol = "aapll";
		final String startDate = "2017-01-02";
		final String endDate = "2017-01-05";
		final String expected = "{\"Ticker\":\"aapll\",\"Error\":\"Invalid Ticker Symbol. No data for it.\"}";

		JsonObject closePrice = service.getClosePrice(tickersymbol, startDate, endDate);
		String actual = Utils.jsonObjectToString(closePrice);
		assertEquals(expected, actual);
	}

	@Test
	public void test200MDASuccess() throws Exception {
		final String tickersymbol = "aapl";
		final String startDate = "2017-01-02";
		final String expected = "{\"200dma\":{\"Ticker\":\"AAPL\",\"Avg\":\"145.66\"}}";

		JsonObject closePrice = service.get200DMA(tickersymbol, startDate);
		String actual = Utils.jsonObjectToString(closePrice);
		assertEquals(expected, actual);
	}

	@Test
	public void test200MDANotFound() throws Exception {
		final String tickersymbol = "aapll";
		final String startDate = "2017-01-02";
		final String expected = "{\"Ticker\":\"aapll\",\"Error\":\"Invalid Ticker Symbol. No data for it.\"}";

		JsonObject closePrice = service.get200DMA(tickersymbol, startDate);
		String actual = Utils.jsonObjectToString(closePrice);
		assertEquals(expected, actual);
	}

	// @Test
	public void test200MDAs1000SymbolsSuccess() throws Exception {
		final String startDate = "2017-01-02";
		final String inputSymbols = new String(
				Files.readAllBytes(Paths.get(ClassLoader.getSystemResource("1000TickerSymbolsInput.txt").toURI())));
		final String expectedResponse = new String(
				Files.readAllBytes(Paths.get(ClassLoader.getSystemResource("1000TickerSymbolsOutput.txt").toURI())));

		JsonObject closePrice = service.get200MDAsForSymbolList(inputSymbols, startDate);
		String actual = Utils.jsonObjectToString(closePrice);
		assertEquals(expectedResponse, actual);
	}

}
