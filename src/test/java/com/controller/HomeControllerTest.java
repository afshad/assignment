package com.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.Utils;
import com.service.PriceService;

/**
 * These tests make sure that the controller is returning what the service is
 * sending it. All possible conditions for the acceptance criteria and more are
 * tested.
 *
 * @author afshad
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc()
@DirtiesContext
public class HomeControllerTest {

	@MockBean
	private PriceService service;

	@Autowired
	private MockMvc mvc;

	@Test
	public void testGetClosePriceSuccess() throws Exception {
		final String tickersymbol = "aapl";
		final String startDate = "2017-01-02";
		final String endDate = "2017-01-05";
		final String result = "{\"Prices\":[{\"Ticker\":\"AAPL\",\"DateClose\":"
				+ "[[\"2017-01-03\",\"116.15\"],[\"2017-01-04\",\"116.02\"],[\"2017-01-05\",\"116.61\"]]}]}";

		when(service.getClosePrice(tickersymbol, startDate, endDate)).thenReturn(Utils.stringToJsonObject(result));

		final MvcResult mvcResult = mvc.perform(get("/api/v2/aapl/closePrice?startDate=2017-01-02&endDate=2017-01-05"))
				.andExpect(status().isOk()).andReturn();
		assertEquals(result, mvcResult.getResponse().getContentAsString());
	}

	@Test
	public void testGetClosePriceNotFound() throws Exception {
		final String tickersymbol = "aapll";
		final String startDate = "2017-01-02";
		final String endDate = "2017-01-05";
		final String result = "{\"Ticker\":\"aapll\",\"Error\":\"Invalid Ticker Symbol. No data for it.\"}";

		when(service.getClosePrice(tickersymbol, startDate, endDate)).thenReturn(Utils.stringToJsonObject(result));

		final MvcResult mvcResult = mvc.perform(get("/api/v2/aapll/closePrice?startDate=2017-01-02&endDate=2017-01-05"))
				.andExpect(status().isNotFound()).andReturn();
		assertEquals(result, mvcResult.getResponse().getContentAsString());
	}

	@Test
	public void testGetClosePriceInvalidDateFormat() throws Exception {
		final String tickersymbol = "aapl";
		final String startDate = "01-02-2017";
		final String endDate = "2017-01-05";
		final String result = "{\"Ticker\":\"aapl\",\"Error\":\"Invalid date format for start/end date.\"}";

		when(service.getClosePrice(tickersymbol, startDate, endDate)).thenReturn(Utils.stringToJsonObject(result));

		final MvcResult mvcResult = mvc.perform(get("/api/v2/aapl/closePrice?startDate=01-02-2017&endDate=2017-01-05"))
				.andExpect(status().isNotFound()).andReturn();
		assertEquals(result, mvcResult.getResponse().getContentAsString());

	}

	@Test
	public void testGetClosePriceInvalidDateRange() throws Exception {
		final String tickersymbol = "aapl";
		final String startDate = "2017-01-05";
		final String endDate = "2017-01-02";
		final String result = "{\"Ticker\":\"aapl\",\"Error\":\"Invalid date format for start/end date.\"}";

		when(service.getClosePrice(tickersymbol, startDate, endDate)).thenReturn(Utils.stringToJsonObject(result));

		final MvcResult mvcResult = mvc.perform(get("/api/v2/aapl/closePrice?startDate=2017-01-05&endDate=2017-01-02"))
				.andExpect(status().isNotFound()).andReturn();
		assertEquals(result, mvcResult.getResponse().getContentAsString());
	}

	@Test
	public void test200MDASuccess() throws Exception {
		final String tickersymbol = "aapl";
		final String startDate = "2017-01-02";
		final String result = "{\"200dma\":{\"Ticker\":\"AAPL\",\"Avg\":\"145.66\"}}";

		when(service.get200DMA(tickersymbol, startDate)).thenReturn(Utils.stringToJsonObject(result));

		final MvcResult mvcResult = mvc.perform(get("/api/v2/aapl/200dma?startDate=2017-01-02"))
				.andExpect(status().isOk()).andReturn();
		assertEquals(result, mvcResult.getResponse().getContentAsString());
	}

	@Test
	public void test200MDANotFound() throws Exception {
		final String tickersymbol = "aapll";
		final String startDate = "2017-01-02";
		final String result = "{\"Ticker\":\"aapll\",\"Error\":\"Invalid Ticker Symbol. No data for it.\"}";

		when(service.get200DMA(tickersymbol, startDate)).thenReturn(Utils.stringToJsonObject(result));

		final MvcResult mvcResult = mvc.perform(get("/api/v2/aapll/200dma?startDate=2017-01-02"))
				.andExpect(status().isNotFound()).andReturn();
		assertEquals(result, mvcResult.getResponse().getContentAsString());
	}

	@Test
	public void test200MDAForSymbolNoDataStartDate() throws Exception {
		final String tickersymbol = "fb";
		final String startDate = "2011-01-02";
		final String result = "{\"Ticker\":\"fb\",\"Requested start date\":\"2011-01-02\",\"First possible start date\":\"2012-05-18\"}";

		when(service.get200DMA(tickersymbol, startDate)).thenReturn(Utils.stringToJsonObject(result));

		final MvcResult mvcResult = mvc.perform(get("/api/v2/fb/200dma?startDate=2011-01-02"))
				.andExpect(status().isOk()).andReturn();
		assertEquals(result, mvcResult.getResponse().getContentAsString());
	}

	@Test
	public void test200MDAsForSymbolListSuccess() throws Exception {
		final String startDate = "2017-01-02";
		final String result = "{\"200dmaList\":[{\"Ticker\":\"AAPL\",\"Avg\":\"145.66\"},"
				+ "{\"Ticker\":\"GM\",\"Avg\":\"36.22\"},{\"Ticker\":\"FB\",\"Avg\":\"151.15\"}]}";

		when(service.get200MDAsForSymbolList("AAPL,GM,FB", startDate)).thenReturn(Utils.stringToJsonObject(result));

		final MvcResult mvcResult = mvc.perform(get("/api/v2/200dmas?startDate=2017-01-02&tickersymbols=AAPL,GM,FB"))
				.andExpect(status().isOk()).andReturn();
		assertEquals(result, mvcResult.getResponse().getContentAsString());
	}

	@Test
	public void test200MDAsForSymbolListErrors() throws Exception {
		final String startDate = "2017-01-02";
		final String result = "{\"200dmaList\":[{\"Ticker\":\"aappl\",\"Avg\":\"Invalid Ticker Symbol. No data for it.\"},"
				+ "{\"Ticker\":\"GM\",\"Avg\":\"36.22\"},{\"Ticker\":\"FB\",\"Avg\":\"151.15\"}]}";

		when(service.get200MDAsForSymbolList("AAPPL,GM,FB", startDate)).thenReturn(Utils.stringToJsonObject(result));

		final MvcResult mvcResult = mvc.perform(get("/api/v2/200dmas?startDate=2017-01-02&tickersymbols=AAPPL,GM,FB"))
				.andExpect(status().isOk()).andReturn();
		assertEquals(result, mvcResult.getResponse().getContentAsString());
	}

	@Test
	public void test200MDAs1000SymbolsSuccess() throws Exception {
		final String startDate = "2017-01-02";
		final String inputSymbols = new String(
				Files.readAllBytes(Paths.get(ClassLoader.getSystemResource("1000TickerSymbolsInput.txt").toURI())));
		final String expectedResponse = new String(
				Files.readAllBytes(Paths.get(ClassLoader.getSystemResource("1000TickerSymbolsOutput.txt").toURI())));

		when(service.get200MDAsForSymbolList(inputSymbols, startDate))
				.thenReturn(Utils.stringToJsonObject(expectedResponse));

		final MvcResult mvcResult = mvc
				.perform(get("/api/v2/200dmas?startDate=2017-01-02&tickersymbols=" + inputSymbols))
				.andExpect(status().isOk()).andReturn();
		assertEquals(expectedResponse, mvcResult.getResponse().getContentAsString());
	}

	@Test
	public void test200MDAs1002Symbols() throws Exception {
		final String startDate = "2017-01-02";
		final String inputSymbols = new String(
				Files.readAllBytes(Paths.get(ClassLoader.getSystemResource("1002TickerSymbolsInput.txt").toURI())));
		final String result = "{\"Error\":\"Number of ticker symbols input must be no greater than 1000.\"}";

		when(service.get200MDAsForSymbolList(inputSymbols, startDate)).thenReturn(Utils.stringToJsonObject(result));

		final MvcResult mvcResult = mvc
				.perform(get("/api/v2/200dmas?startDate=2017-01-02&tickersymbols=" + inputSymbols))
				.andExpect(status().isNotFound()).andReturn();
		assertEquals(result, mvcResult.getResponse().getContentAsString());
	}

}
