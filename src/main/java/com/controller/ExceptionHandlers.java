package com.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;

/**
 * The global exception handling class.
 * @author afshad
 *
 */
//@ControllerAdvice
public class ExceptionHandlers {
	Logger LOGGER = Logger.getLogger( ExceptionHandlers.class.getName() );
	
	/**
	 * Handle a 404 exception upon invoking a REST call.
	 * @param ex The exception.
	 * @return The response message for the user as a String.
	 */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public String handleAnyOtherException(final Exception ex) {
//	    String response = ex.getResponseBodyAsString();
//		if(response.contains("QECx02")) {
//	    	String message = "404 - Invalid Ticker Symbol."; 
//	    	LOGGER.error(message);
//	    	return message;
//	    } else if(response.contains("QESx04")) {
//	    	String message = "404 - Invalid date format for start/end date."; 
//	    	LOGGER.error(message);
//	    	return message;
//	    }  

        return "Error";
    }

}