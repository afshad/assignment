package com.controller;

import java.io.StringWriter;
import java.util.StringTokenizer;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.service.PriceService;

@RestController
@RequestMapping("/api/v2")
public class HomeController {
	private static final Logger LOGGER = Logger.getLogger(HomeController.class.getName());

	@Autowired
	private PriceService priceService;

	/**
	 * Answer No 1. Get the Close Price for a ticker symbol for a range of
	 * dates.
	 *
	 * @param tickersymbol
	 *            The ticker symbol.
	 * @param startDate
	 *            The start date.
	 * @param endDate
	 *            The end date.
	 * @return The response containing the close prices.
	 */
	@RequestMapping(value = "/{tickersymbol}/closePrice", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> getClosePriceForSymbolByDateRange(@PathVariable("tickersymbol") String tickersymbol,
			@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate) {

		HttpStatus responseStatus = HttpStatus.OK;
		JsonObject json = priceService.getClosePrice(tickersymbol, startDate, endDate);

		try {
			String string = json.getString("Error");
			responseStatus = HttpStatus.NOT_FOUND;
			LOGGER.info(string);
		} catch (NullPointerException e) {
		}

		return new ResponseEntity<Object>(jsonObjectToString(json), responseStatus);
	}

	/**
	 * Answer No 2. Get the 200 day moving average price for a ticker symbol
	 * beginning with a start date.
	 *
	 * @param tickersymbol
	 *            The ticker symbol.
	 * @param startDate
	 *            The start date.
	 * @return The response containing the close prices.
	 */
	@RequestMapping(value = "/{tickersymbol}/200dma", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> get200MDAForSymbolFromStartDate(@PathVariable("tickersymbol") String tickersymbol,
			@RequestParam("startDate") String startDate) {

		HttpStatus responseStatus = HttpStatus.OK;
		JsonObject json = priceService.get200DMA(tickersymbol, startDate);

		try {
			String string = json.getString("Error");
			responseStatus = HttpStatus.NOT_FOUND;
			LOGGER.info(string);
		} catch (NullPointerException e) {
		}

		return new ResponseEntity<Object>(jsonObjectToString(json), responseStatus);
	}

	/**
	 * Answer No 3. Get the 200 day moving average price for upto 1000 ticker
	 * symbols beginning with a start date.
	 *
	 * @param startDate
	 *            The start date.
	 * @param tickersymbols
	 *            The ticker symbols.
	 * @return The response containing the 200mda's for the input ticker
	 *         symbols. An invalid ticker symbol generates a message in the JSON
	 *         response that there is no data for it. If there is no data for a
	 *         ticker symbol with the start date provided, data for the first
	 *         possible start date is provided back to the client.
	 */
	@RequestMapping(value = "/200dmas", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> get200MDAsForSymbolList(@RequestParam("startDate") String startDate,
			@RequestParam("tickersymbols") String tickersymbols) {

		HttpStatus responseStatus = HttpStatus.OK;
		JsonObject result = null;

		StringTokenizer st = new StringTokenizer(tickersymbols, ",");
		if (st.countTokens() > 1000) {
			JsonObjectBuilder error = Json.createObjectBuilder();
			error.add("Error", "Number of ticker symbols input must be no greater than 1000.");
			responseStatus = HttpStatus.NOT_FOUND;
			result = error.build();
		} else {
			result = priceService.get200MDAsForSymbolList(tickersymbols, startDate);
		}

		return new ResponseEntity<Object>(jsonObjectToString(result), responseStatus);
	}

	public static String jsonObjectToString(JsonObject obj) {
		StringWriter writer = new StringWriter();
		JsonWriter jsonWriter = Json.createWriter(writer);
		jsonWriter.writeObject(obj);
		jsonWriter.close();
		return writer.toString();
	}

}