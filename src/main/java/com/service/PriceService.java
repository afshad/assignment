package com.service;

import javax.json.JsonObject;

public interface PriceService {

	public JsonObject getClosePrice(String tickersymbol, String startDate, String endDate);

	public JsonObject get200DMA(String tickersymbol, String startDate);

	public JsonObject get200MDAsForSymbolList(String tickerList, String startDate);
}
