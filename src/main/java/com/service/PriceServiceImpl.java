package com.service;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.ehcache.EhCacheCache;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.domain.Dataset;
import com.domain.TickerPrices;

/**
 * Service class that holds the business logic.
 *
 * @author afshad
 *
 */
@Service
@CacheConfig(cacheNames = "StockPrices")
public class PriceServiceImpl implements PriceService {

	private static final Logger LOGGER = Logger.getLogger(PriceServiceImpl.class.getName());

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	private CacheManager cacheManager;

	public PriceServiceImpl() {
		clearCache();
	}

	@CacheEvict(allEntries = true)
	public void clearCache() {

	}

	/**
	 * Call the quandl API to get the Close Price for a ticker symbol for a
	 * range of dates.
	 *
	 * @param tickersymbol
	 *            Ticker symbol.
	 * @param startDate
	 *            Start date.
	 * @param endDate
	 *            End date.
	 * @return Json representing the close prices.
	 */
	@Override
	@Cacheable
	public JsonObject getClosePrice(String tickersymbol, String startDate, String endDate) {
		// LOGGER.info("ticker: " + tickersymbol + " startDate: " + startDate +
		// " endDate: " + endDate);
		EhCacheCache e = (EhCacheCache) cacheManager.getCache("StockPrices");
		LOGGER.info("Ehcache size=" + e.getNativeCache().getSize());
		final StringBuilder sb = new StringBuilder();
		sb.append("https://www.quandl.com/api/v3/datasets/WIKI/");
		sb.append(tickersymbol + ".json");
		sb.append("?");
		sb.append("start_date=");
		sb.append(startDate);
		sb.append("&");
		sb.append("end_date=");
		sb.append(endDate);

		TickerPrices quote = null;
		try {
			quote = restTemplate.getForObject(sb.toString(), TickerPrices.class);
		} catch (final HttpClientErrorException ex) {
			final String message = checkQuandlErrors(ex.getResponseBodyAsString());
			LOGGER.error(message);
			return createJsonObjectError(tickersymbol, message);
		}

		final Dataset dataset = quote.getDataset();
		final List<List<String>> data = dataset.getData();

		final JsonArrayBuilder dateCloseArrayBuilder = Json.createArrayBuilder();
		for (int i = data.size() - 1; i >= 0; i--) {
			final List<String> info = data.get(i);
			final String date = info.get(0);
			final String closePrice = info.get(4);
			final JsonArrayBuilder dateCloseSubArrayBuilder = Json.createArrayBuilder();
			dateCloseSubArrayBuilder.add(date);
			dateCloseSubArrayBuilder.add(closePrice);
			dateCloseArrayBuilder.add(dateCloseSubArrayBuilder);
		}

		final JsonObjectBuilder tickerInfo = Json.createObjectBuilder();
		tickerInfo.add("Ticker", dataset.getDatasetCode()).add("DateClose", dateCloseArrayBuilder);

		final JsonArrayBuilder tickerInfoArray = Json.createArrayBuilder();
		tickerInfoArray.add(tickerInfo);

		final JsonObjectBuilder prices = Json.createObjectBuilder();
		prices.add("Prices", tickerInfoArray);

		final JsonObject json = prices.build();
		return json;
	}

	/**
	 * Get the 200 Day moving average for a ticker symbol beggining with a start
	 * date. This method calls the Quandl REST api to achieve this.
	 *
	 * @param tickersymbol
	 *            Ticker symbol.
	 * @param startDate
	 *            Start date.
	 * @return Json representing the 200 Day moving average.
	 */
	@Override
	@Cacheable
	public JsonObject get200DMA(String tickersymbol, String startDate) {
		final RestTemplate restTemplate = new RestTemplate();
		final StringBuilder sb = new StringBuilder();
		sb.append("https://www.quandl.com/api/v3/datasets/WIKI/");
		sb.append(tickersymbol + ".json");
		sb.append("?");
		sb.append("column_index=4");
		sb.append("&");
		sb.append("start_date=");
		sb.append(startDate);
		sb.append("&");
		sb.append("collapse=daily");
		sb.append("&");
		sb.append("order=asc");
		sb.append("&");
		sb.append("limit=200");

		TickerPrices quote = null;
		try {
			quote = restTemplate.getForObject(sb.toString(), TickerPrices.class);
		} catch (final HttpClientErrorException ex) {
			final String message = checkQuandlErrors(ex.getResponseBodyAsString());
			LOGGER.error(message);
			return createJsonObjectError(tickersymbol, message);
		}

		final Dataset dataset = quote.getDataset();

		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			final Date sDate = sdf.parse(startDate);
			final Date oldestDate = sdf.parse(dataset.getOldestAvailableDate());
			if (oldestDate.after(sDate)) {
				final String message = "No data available for start date.";
				LOGGER.error(message);
				final JsonObjectBuilder prices = Json.createObjectBuilder();
				prices.add("Ticker", tickersymbol).add("Requested start date", startDate)
						.add("First possible start date", dataset.getOldestAvailableDate());
				return prices.build();
			}
		} catch (final ParseException e1) {
		}

		final List<List<String>> data = dataset.getData();
		final String dma = calculate200DMA(data);
		final JsonObjectBuilder tickerInfo = Json.createObjectBuilder();
		tickerInfo.add("Ticker", dataset.getDatasetCode()).add("Avg", dma);
		final JsonObjectBuilder prices = Json.createObjectBuilder();
		prices.add("200dma", tickerInfo);

		final JsonObject json = prices.build();
		return json;
	}

	/**
	 * Get the 200 Day moving average for a ticker symbol beggining with a start
	 * date. This method calls the Quandl REST api to achieve this.
	 *
	 * @param tickerList
	 *            String of ticker symbols separated by "," (comma).
	 * @param startDate
	 *            Start date.
	 * @return Json representing the 200 Day moving average.
	 */
	@Override
	@Cacheable
	public JsonObject get200MDAsForSymbolList(String tickerList, String startDate) {
		final StringTokenizer st = new StringTokenizer(tickerList, ",");
		final JsonArrayBuilder array = Json.createArrayBuilder();
		final JsonObjectBuilder results = Json.createObjectBuilder();
		JsonObject result = null;

		while (st.hasMoreTokens()) {
			boolean error = false;
			final String tickersymbol = st.nextToken();
			final RestTemplate restTemplate = new RestTemplate();
			final StringBuilder sb = new StringBuilder();
			sb.append("https://www.quandl.com/api/v3/datasets/WIKI/");
			sb.append(tickersymbol + ".json");
			sb.append("?");
			sb.append("column_index=4");
			sb.append("&");
			sb.append("start_date=");
			sb.append(startDate);
			sb.append("&");
			sb.append("collapse=daily");
			sb.append("&");
			sb.append("order=asc");
			sb.append("&");
			sb.append("limit=200");
			LOGGER.error(tickersymbol);

			TickerPrices quote = null;
			try {
				quote = restTemplate.getForObject(sb.toString(), TickerPrices.class);
			} catch (final HttpClientErrorException ex) {
				error = true;
				final String message = checkQuandlErrors(ex.getResponseBodyAsString());
				LOGGER.error(message);
				result = createJsonObjectError(tickersymbol, message);
			}

			if (!error) {
				final Dataset dataset = quote.getDataset();
				final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try {
					final Date sDate = sdf.parse(startDate);
					final Date oldestDate = sdf.parse(dataset.getOldestAvailableDate());
					if (oldestDate.after(sDate)) {
						error = true;
						final String message = "No data available for start .";
						LOGGER.error(message);
						final JsonObjectBuilder prices = Json.createObjectBuilder();
						prices.add("Ticker", tickersymbol).add("Requested start date", startDate)
								.add("First possible start date", dataset.getOldestAvailableDate());
						result = prices.build();
					}
				} catch (final ParseException e1) {
				}

				if (!error) {
					final List<List<String>> data = dataset.getData();
					final String dma = calculate200DMA(data);
					final JsonObjectBuilder tickerInfo = Json.createObjectBuilder();
					tickerInfo.add("Ticker", dataset.getDatasetCode()).add("Avg", dma);
					result = tickerInfo.build();
				}
			}
			array.add(result);
		}
		results.add("200dmaList", array);
		return results.build();
	}

	private String calculate200DMA(List<List<String>> data) {
		double sum = 0.0;
		for (int i = 0; i < data.size(); i++) {
			final List<String> info = data.get(i);
			final String closePrice = info.get(1);
			sum += Double.valueOf(closePrice);
		}
		final double average = sum / 200;
		final DecimalFormat df = new DecimalFormat("###.##");
		return df.format(average);
	}

	private String checkQuandlErrors(String response) {
		String message = null;
		if (response.contains("QECx02")) {
			message = "Invalid Ticker Symbol. No data for it.";
		} else if (response.contains("QESx04")) {
			message = "Invalid date format for start/end date.";
		}
		return message;
	}

	private JsonObject createJsonObjectError(String tickerSymbol, String errorMessage) {
		final JsonObjectBuilder prices = Json.createObjectBuilder();
		prices.add("Ticker", tickerSymbol).add("Error", errorMessage);
		return prices.build();
	}
}
