package com.service;

import java.util.HashMap;
import java.util.LinkedList;

import org.springframework.stereotype.Service;

/**
 * Cache representing the Most Frequently Used ticker symbols. This is needed to
 * reduce latency on REST calls for ticker information. The cache is backed by a
 * list and a map. List representing in order from head to tail the most
 * frequently used ticker symbols. Map contains the ticker symbols and their
 * corresponding close prices.
 *
 * @author afshad
 *
 */
@Service
public class MostFrequentlyUsedCache {
	private int cacheSize = 10;
	// List representing in order from head to tail the most frequently used
	// ticker symbols.
	private final LinkedList<String> list;

	// Map representing the Ticker Symbols and their corresponding close prices.
	private final HashMap<String, String> map;

	/**
	 * Constructor.
	 *
	 * @param cacheSize
	 *            The size of the cache.
	 */
	public MostFrequentlyUsedCache() {
		map = new HashMap<String, String>(cacheSize);
		list = new LinkedList<String>();
	}

	/**
	 * Insert a Ticker symbol and its close price in the cache. <br>
	 * If the cache is full, then the least recently used entry in the cache
	 * will be removed.
	 *
	 * @param key
	 *            The ticker symbol.
	 * @param val
	 *            The close price for the ticker symbol.
	 */
	public void put(String key, String val) {
		// If the cache is full then remove the least frequently used element
		// from it.
		if (list.size() == cacheSize) {
			removeElement();
		}
		list.addFirst(key);
		map.put(key, val);
	}

	/**
	 * Get the closing price of a ticker symbol from the cache.
	 *
	 * @param key
	 *            The ticker symbol whose close price is to be returned.
	 * @return The closing price of a ticker symbol from the cache.
	 */
	public String get(String key) {
		final boolean element = list.remove(key);
		if (element) {
			// Add the element back to the front of the list since it was most
			// recenely used.
			list.addFirst(key);
			return map.get(key);
		}
		return null;
	}

	public void removeEntry(String key) {
		list.remove(key);
		map.remove(key);
	}

	public void printKeyOrder() {
		System.out.println("KeyOrder() " + list + " Cache content: " + map);
	}

	/**
	 * Remove the least recently used item from the cache.
	 */
	private void removeElement() {
		final String key = list.removeLast();
		map.remove(key);
	}

	public void setCacheSize(int i) {
		cacheSize = i;
	}

}
