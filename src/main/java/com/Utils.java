package com;

import java.io.StringReader;
import java.io.StringWriter;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;

/**
 * Utility class
 *
 * @author afshad
 *
 */
public class Utils {

	public static String jsonObjectToString(JsonObject obj) {
		StringWriter writer = new StringWriter();
		JsonWriter jsonWriter = Json.createWriter(writer);
		jsonWriter.writeObject(obj);
		jsonWriter.close();
		return writer.toString();
	}

	public static JsonObject stringToJsonObject(String text) {
		final JsonReader reader = Json.createReader(new StringReader(text));
		final JsonObject resultJson = reader.readObject();
		reader.close();
		return resultJson;
	}

}
