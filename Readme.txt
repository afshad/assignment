1. To build the jar use maven:
mvn clean install 
at the root location of project.

2. To run the application:
java -jar target/data.jar

3. You can make the REST calls with the format as defined in the functional requirements for the exercises.
For exercise 1 example:
http://localhost:8080/api/v2/fb/closePrice?startDate=2017-01-02&endDate=2017-01-05

For exercise 2 example:
http://localhost:8080/api/v2/aapl/200dma?startDate=2017-01-02

For exercise 3 example:
http://localhost:8080/api/v2/200dmas?startDate=2017-01-02&tickersymbols=OEDV,AAPL,BAC

The output is in the format mentioned in the Functional Requirements.

NOTE: As per my judgement and research it seems that the JSON format in the functional requirement for excecise 1 is invalid.
The value for DateClose is a list of lists.
However curly braces are used to specify the outter list which is wrong it should be square braces.

4. Caching:
Currently caching is implemented using Spring caching backed by Ehcache.

The current caching mechanism only caches the data for a particular request. 
For example:
getClosePrice(String tickersymbol, String startDate, String endDate)
So for a particular symbol/startDate/endDate combination, the result will be cached and the next time a request is made,
the result will be sent back to the controller from the service without invoking any code in the service method.

This mechanism needs to be updated so that a cahce of closing prices is kept for each symbol requested and referenced when making new requests.

I have also implemented my own MostFrequentlyUsedCache service that I could use instead of spring however that would require a lot of framework code which spring provides.

5. Returning model objects from service layer instead of JsonObject.
JsonObject was initially returned to get around the functional requirement format for json. Serialization/de-serialization from object to json was not working.
Given more time this updated to use model objects. 

